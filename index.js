const express = require('express')
const port = 2020
let game_assets = require('./db/gamedata.json')
const app = express()

// menggunakan middleware express.json() untuk menangani request body
app.use(express.json())

// 
app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type, Accept,Authorization,Origin");
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    res.setHeader("Access-Control-Allow-Credentials", true);
    next();
});

// home endpoint
app.get('/', (req, res) => {
    let hello = [{
        "msg" : "this route root uri, tes route on postman" 
    }]

    res.json(hello)
}) 

/**
 * Main Page Asset API
 */

/**
 * Game Asset API
 */

// endpoint tampil semua data asset /api/assets
app.get('/api/assets/', (req, res) => {
    // console.log(game_assets)
    res.status(200).json(game_assets)
})

// endpoint detail assets /api/assets/:id
app.get('/api/assets/:id', (req, res) => {
    const asset = game_assets.find(i => i.id === +req.params.id)
    console.log(asset)
    res.status(200).json(asset)
})

// jalankan app di port 2020
app.listen(port, () =>{
    console.log(`Server jalan di port http://localhost:${port}`)
})